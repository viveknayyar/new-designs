$(document).ready(function() {
	var suggestion_engine = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		limit: 10,
		remote: {
			url: 'http://www.craftsvilla.com/v1/getAutosuggestion?term=%QUERY',
			filter: function (response) {
				var categories = [];
				var products = [];
				response.data.map(function(item) {
					var json_data = {
						"type": item.type,
						"text": item.content.text,
						"url_path": item.content.url_path,
						"vendor_name": item.content.vendor_name,
						"discounted_price": item.content.discounted_price,
						"entity_id": item.content.entity_id,
						"image": item.content.images
					};
					if(item.type == 'product') {
						products.push(json_data);
					} else {
						categories.push(json_data);
					}
				});
				var parsed_response = categories.concat(products);
				return parsed_response;
			},
			wildcard: '%QUERY'
		}
	});
	suggestion_engine.initialize();


	$('.typeahead').typeahead({
		minLength: 1,
		highlight: true,
		hint: true,
	},{
		source: suggestion_engine.ttAdapter(),
		displayKey: 'text',
		templates: {
			empty: [
				'<div class="custom_results_text custom_results">',
					'<div class="result_text">No Items Found</div>',
				'</div>'
			].join('\n'),
			suggestion: function(data){
				if(data.type=='product') {
					var text = [
						'<div class="custom_results_image custom_results">',
							'<div class="result_image hidden-xs hidden-sm" style="background-image:url(http://img1.craftsvilla.com/thumb/166x166/' + data.image + ')"></div>',
							'<div class="result_text">' + data.text  + '</div>'
					].join('\n');
					if(data.vendor_name.trim().length) {
						text = text.concat(['<div class="result_text hidden-xs hidden-sm">by ' + data.vendor_name  + '</div>']);
					}
					var other_text = [
						'<div class="result_text hidden-xs hidden-sm"><span class="discount_price">Rs. ' + parseInt(data.discounted_price)  + '</span></div>',
						'<div class="result_type visible-xs visible-sm" id="product_type">  Product </div>',
					'</div>'
					].join('\n');
					return text.concat(other_text);
				} else {
					return ( [
						'<div class="custom_results_text custom_results">',
							'<div class="result_text">' + data.text + '</div>',
							'<div class="result_type"> ' + data.type + '</div>',
						'</div>'
					].join('\n')
					);
				}
			},
		},
	}).bind('typeahead:selected', function(obj, datum) {
		dataLayer.push({
			"event":"SearchPerformedEvent",
			"eventName":"SearchPerformed",
			"source":"homeScreen",
			"suggestionUsed":"yes",
			"searchQuery": datum.text
		});
		if(datum.type=="product") {
			window.location = '/catalog/product/view/id/' + datum.entity_id + '/s/'+ datum.url_path + '/';
		} else {
			window.location = '/' + datum.url_path;
		}
	});
});
